package cn.edu.scau.biubiusuisui.utils.spi.impl;

import cn.edu.scau.biubiusuisui.utils.spi.SayHello;

/**
 * @author Jade Yeung
 * @time 2022/4/30 11:43
 * @since 1.3.0
 */
public class SayChineseImpl implements SayHello {
    @Override
    public String say() {
        return "你好";
    }
}
