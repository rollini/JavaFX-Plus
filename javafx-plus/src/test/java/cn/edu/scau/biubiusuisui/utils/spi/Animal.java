package cn.edu.scau.biubiusuisui.utils.spi;

/**
 * @author Jade Yeung
 * @time 2022/4/30 11:53
 * @since 1.3.0
 */
public interface Animal {
    String say();
}
