package cn.edu.scau.biubiusuisui.utils.spi;

/**
 * 测试用接口
 *
 * @author Jade Yeung
 * @time 2022/4/30 11:41
 * @since 1.3.0
 */
public interface SayHello {
    String say();
}
