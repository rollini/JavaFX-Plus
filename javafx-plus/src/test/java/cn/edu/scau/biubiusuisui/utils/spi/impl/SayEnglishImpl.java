package cn.edu.scau.biubiusuisui.utils.spi.impl;

import cn.edu.scau.biubiusuisui.utils.spi.SayHello;

/**
 * @author Jade Yeung
 * @time 2022/4/30 11:44
 * @since 1.3.0
 */
public class SayEnglishImpl implements SayHello {
    @Override
    public String say() {
        return "Hello";
    }
}
