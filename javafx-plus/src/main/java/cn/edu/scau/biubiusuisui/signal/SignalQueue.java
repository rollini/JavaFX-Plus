package cn.edu.scau.biubiusuisui.signal;

import cn.edu.scau.biubiusuisui.annotation.FXReceiver;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.entity.FXMethodEntity;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import cn.edu.scau.biubiusuisui.utils.ProxyUtil;
import javafx.application.Platform;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 信号队列
 *
 * @author jack
 * @author Jade Yeung
 * @time 2019/6/25 12:24
 * @time 2022/4/30 17:31
 * @since 1.3.0
 */
public class SignalQueue {
    private static final IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(SignalQueue.class);

    private static Map<String, List<FXMethodEntity>> receivers = new ConcurrentHashMap<>();  //Map<主题，订阅了主题的所有方法>

    private static SignalQueue signalQueue = null;

    protected SignalQueue() {
    }

    /**
     * 获取单例
     *
     * @return SignalQueue
     */
    public static synchronized SignalQueue getInstance() {
        if (signalQueue == null) {
            signalQueue = new SignalQueue();
        }
        return signalQueue;
    }

    /**
     * @param fxBaseController 基础controller代理
     * @description 注册消费者，即FXReceiver注解的method
     */
    public void registerConsumer(FXBaseController fxBaseController) {
        Method[] methods = ProxyUtil.getTargetClass(fxBaseController).getDeclaredMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (FXReceiver.class.equals(annotation.annotationType())) {
                    logger.info("registering consumer: " + fxBaseController.getControllerName());
                    FXReceiver receiver = (FXReceiver) annotation;
                    FXMethodEntity fxMethodEntity = new FXMethodEntity(fxBaseController, method);

                    for (String name : receiver.name()) {
                        List<FXMethodEntity> fxMethodEntities = receivers.get(name);
                        if (fxMethodEntities == null) {
                            fxMethodEntities = new ArrayList<>();
                        }
                        fxMethodEntities.add(fxMethodEntity);
                        receivers.put(name, fxMethodEntities);
                    }
                }
            }
        }
    }

    /**
     * 处理信号发送
     *
     * @param nameList 信号ID列表
     * @param msg      消息内容
     * @since 1.3.0 name->nameList，支持多信号发送
     */
    public void sendMsg(List<String> nameList, Object msg) {
        nameList.parallelStream().forEach(name -> {
            List<FXMethodEntity> fxMethodList = receivers.get(name);
            if (Objects.nonNull(fxMethodList)) {
                fxMethodList.parallelStream().forEach(fxMethodEntity -> Platform.runLater(() -> handleSendMsg(fxMethodEntity, msg)));
            }
        });
    }

    private void handleSendMsg(FXMethodEntity fxMethodEntity, Object msg) {
        Method method = fxMethodEntity.getMethod();
        method.setAccessible(true);
        FXBaseController fxBaseController = fxMethodEntity.getFxBaseController();
        if (method.getParameterCount() == 0) {
            try {
                method.invoke(fxBaseController);
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error("", e);
            }
        } else {
            try {
                // 调起FXReceiver注解的方法
                method.invoke(fxBaseController, msg);
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error("", e);
            }
        }
    }
}
