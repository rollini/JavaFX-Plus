package cn.edu.scau.biubiusuisui.factory;

import cn.edu.scau.biubiusuisui.annotation.FXBind;
import cn.edu.scau.biubiusuisui.annotation.FXController;
import cn.edu.scau.biubiusuisui.annotation.FXData;
import cn.edu.scau.biubiusuisui.config.FXMLLoaderPlus;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.entity.FXPlusContext;
import cn.edu.scau.biubiusuisui.exception.NoSuchChangeMethod;
import cn.edu.scau.biubiusuisui.exception.NotFXControllerException;
import cn.edu.scau.biubiusuisui.expression.data.ExpressionParser;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import cn.edu.scau.biubiusuisui.proxy.FXControllerProxy;
import cn.edu.scau.biubiusuisui.signal.SignalQueue;
import cn.edu.scau.biubiusuisui.stage.StageManager;
import cn.edu.scau.biubiusuisui.utils.ProxyUtil;
import cn.edu.scau.biubiusuisui.utils.StringUtil;
import javafx.collections.ObservableMap;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/25 8:12
 * @since JavaFX2.0 JDK1.8
 */
public class FXControllerFactory {
    private static IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(FXControllerFactory.class);

    private static final BeanBuilder BEAN_BUILDER = new FXBuilder();

    /**
     * 控制类的注入流程
     * 这是一个即将被创建的控制类
     * <pre>
     *   <code>
     *    class MainController{
     *          @Autowired
     *          Student stu; //普通属性
     *          @FXML
     *          Button btn; //FX属性
     *    }
     *   </code>
     *   </pre>
     * 1. 实现对普通属性的注入
     * 如果使用了Spring那么这类会自动注入那些@Autowired的属性，请不要将这个方法用在控制器属性中
     * <pre>
     *   <code>
     *    class MainController{
     *          @Autowired
     *          Student stu ;  //初始化完成
     *          @FXML
     *          Button btn;  // null
     *    }
     *   </code>
     *   </pre>
     * 2. 通过loadFXML实现对FX属性的注入
     * <pre>
     *   <code>
     *    class MainController{
     *          @Autowired
     *          Student stu ;  //初始化完成
     *          @FXML
     *          Button btn;  // 初始化完成
     *    }
     *   </code>
     *   </pre>
     * <p>
     * 3.  完成对FXBind的注解的解析
     * <p>
     * <p>
     * <p>
     * 4.  完成注册
     *
     * @param clazz          instance that extends by FXBaseController
     * @param controllerName
     * @return
     */
    private static FXBaseController getFxBaseController(Class<?> clazz, String controllerName, BeanBuilder beanBuilder) {
        return getFxBaseController0(clazz, controllerName, beanBuilder);
    }

    private static FXBaseController getFxBaseController0(Class<?> clazz, String controllerName, BeanBuilder beanBuilder) {
        if (Objects.nonNull(clazz.getDeclaredAnnotation(FXController.class))) {
            logger.info("loading the FXML file of " + clazz.getName());

            // create a cn.edu.scau.biubiusuisui.proxy for monitoring methods
            FXBaseController fxBaseController = (FXBaseController) beanBuilder.getBean(clazz); //获取controller实例
            if (Objects.nonNull(fxBaseController)) {
                if (StringUtil.isNoneBlank(controllerName)) {
                    fxBaseController.setName(controllerName);
                }

                //产生代理从而实现赋能
                FXBaseController fxControllerProxy = new FXControllerProxy(fxBaseController).getEnhancer();

                parseFXData(fxControllerProxy);

                ObservableMap<String, Object> namespace = fxControllerProxy.getFxmlLoader().getNamespace();
                addDataInNameSpace(namespace, fxControllerProxy); //处理fxBaseController里面的@FXData
                scanBind(namespace, fxControllerProxy); //处理@FXBind

                register(fxControllerProxy);

                return fxControllerProxy;
            }
        }
        return null;
    }

    /**
     * 为FXMLLoaderPlus设值
     *
     * @param fxmlLoader       FXMLLoaderPlus
     * @param fxBaseController 基类的代理
     */
    private static void enhanceFxmlLoader(FXMLLoaderPlus fxmlLoader, /*FXBaseController fxBaseController, delete in 1.3.0, just need proxy */ FXBaseController fxBaseController) {
        fxmlLoader.setRoot(fxBaseController);
        fxmlLoader.setController(fxBaseController);
        fxmlLoader.setBaseController(fxBaseController);
    }

    /**
     * 将代理对象和目标对象注册
     *
     * @param fxBaseController 代理对象
     */
    private static void register(/*FXBaseController fxBaseController, delete in 1.3.0, just need proxy*/ FXBaseController fxBaseController) {
        FXPlusContext.registerController(fxBaseController); //保存
        SignalQueue.getInstance().registerConsumer(fxBaseController); // 添加进入信号队列 信号功能
        if (fxBaseController.isWindow()) {
            StageManager.getInstance().registerWindow(fxBaseController);
        }
    }

    private FXControllerFactory() {

    }

    /**
     * 加载舞台
     * 原函数名为loadMainStage(Class<?> clazz, BeanBuilder beanBuilder)
     *
     * @param clazz
     * @param beanBuilder
     */
    public static void loadStage(Class<?> clazz, BeanBuilder beanBuilder) throws NotFXControllerException {
        FXBaseController fxController = getFXController(clazz, null, beanBuilder);
        if (Objects.isNull(fxController)) {
            throw new NotFXControllerException(clazz.getName());
        }
        if (fxController.isMainStage()) {
            fxController.showStage();
        }
    }

    public static FXBaseController getFXController(Class<?> clazz) {
        return getFXController(clazz, BEAN_BUILDER);
    }

    public static FXBaseController getFXController(Class<?> clazz, BeanBuilder beanBuilder) {
        return getFXController(clazz, null, beanBuilder);
    }

    public static FXBaseController getFXController(Class<?> clazz, String controllerName) {
        return getFXController(clazz, controllerName, BEAN_BUILDER);
    }

    public static FXBaseController getFXController(Class<?> clazz, String controllerName, BeanBuilder beanBuilder) {
        return getFxBaseController(clazz, controllerName, beanBuilder);
    }

    /**
     * 解析FXData注解
     *
     * @param fxBaseController
     */
    private static void parseFXData(FXBaseController fxBaseController) {
        Field[] fields = ProxyUtil.getTargetClass(fxBaseController).getDeclaredFields();
        for (Field field : fields) {
            FXData annotation = field.getAnnotation(FXData.class);
            if (Objects.nonNull(annotation)) {
                field.setAccessible(true);
                //建立代理
                try {
                    Object fieldValue = field.get(fxBaseController);
                    Object fieldValueProxy = FXEntityFactory.wrapFXBean(fieldValue);
                    field.set(fxBaseController, fieldValueProxy);
                } catch (IllegalAccessException e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 将FXData中的数据绑定到namespace
     *
     * @param namespace
     * @param fxBaseController
     */
    private static void addDataInNameSpace(ObservableMap<String, Object> namespace, Object fxBaseController) {
        Field[] fields = ProxyUtil.getTargetClass(fxBaseController).getDeclaredFields();
        for (Field field : fields) {
            FXData annotation = field.getAnnotation(FXData.class);
            if (Objects.nonNull(annotation)) {
                try {
                    field.setAccessible(true);
                    String fx_id;
                    //field.setAccessible(true);
                    if (StringUtil.isBlank(annotation.fx_id())) {
                        fx_id = field.getName();
                    } else {
                        fx_id = annotation.fx_id();
                    }
                    Object fieldValue = field.get(fxBaseController);
                    namespace.put(fx_id, fieldValue);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 扫描FXBind注解
     *
     * @param namespace
     * @param fxBaseController
     */
    private static void scanBind(ObservableMap<String, Object> namespace, FXBaseController fxBaseController) {
        Field[] fields = ProxyUtil.getTargetClass(fxBaseController).getDeclaredFields();
        for (Field field : fields) {
            parseBind(namespace, fxBaseController, field);
        }
    }

    /**
     * 解析FXBind注解
     *
     * @param namespace
     * @param fxBaseController
     * @param field
     */
    private static void parseBind(ObservableMap<String, Object> namespace, FXBaseController fxBaseController, Field field) {
        FXBind fxBind = field.getAnnotation(FXBind.class);
        if (fxBind != null) {
            ExpressionParser<FXBaseController> expressionParser = new ExpressionParser<>(namespace, fxBaseController);
            field.setAccessible(true);
            String[] expressions = fxBind.value();
            try {
                Object fieldValue = field.get(fxBaseController);
                for (String e : expressions) {
                    expressionParser.parse(fieldValue, e);
                }
            } catch (IllegalAccessException | NoSuchChangeMethod e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
