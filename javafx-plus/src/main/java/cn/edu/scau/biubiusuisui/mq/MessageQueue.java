package cn.edu.scau.biubiusuisui.mq;

import cn.edu.scau.biubiusuisui.signal.SignalQueue;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/25 12:24
 * @since JavaFX2.0 JDK1.8
 * @deprecated will be deleted in 1.4.0，renamed as {@link cn.edu.scau.biubiusuisui.signal.SignalQueue}
 */
public class MessageQueue extends SignalQueue {
}
