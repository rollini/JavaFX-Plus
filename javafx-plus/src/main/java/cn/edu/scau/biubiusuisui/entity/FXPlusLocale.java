package cn.edu.scau.biubiusuisui.entity;

import java.util.Locale;

/**
 * @author suisui
 * @version 1.2
 * @description JavaFX的Locale枚举类型
 * @date 2020/5/3 10:47
 * @since JDK1.8 JavaFX2.0
 */
public enum FXPlusLocale {
    /**
     * 不设置
     */
    NONE(null),

    /**
     * 简体中文
     */
    SIMPLIFIED_CHINESE(Locale.SIMPLIFIED_CHINESE),

    /**
     * 繁体中文
     */
    TRADITIONAL_CHINESE(Locale.TRADITIONAL_CHINESE),

    /**
     * English          英语
     */
    ENGLISH(Locale.ENGLISH),


    /**
     * Le français      法语
     */
    FRENCH(Locale.FRENCH),


    /**
     * Deutsch          德语
     */
    GERMAN(Locale.GERMAN),

    /**
     * lingua italiana  意大利语
     */
    ITALIAN(Locale.ITALIAN),


    /**
     * 日本人            日语
     */
    JAPANESE(Locale.JAPANESE),


    /**
     * 한국어            韩语
     */
    KOREAN(Locale.KOREAN);

    private final Locale locale;

    FXPlusLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

}
