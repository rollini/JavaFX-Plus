package cn.edu.scau.biubiusuisui.root;

/**
 * 代理接口
 *
 * @author Jade Yeung
 * @time 2022/5/3 11:14
 * @since 1.3.0
 */
public interface IFXProxy<T> {
    /**
     * 通过代理类，获取实际被代理的类
     *
     * @return T 被代理类
     */
    T getTarget();

    /**
     * 构造出增强代理类
     *
     * @return T 增强的代理类
     */
    T getEnhancer();
}
