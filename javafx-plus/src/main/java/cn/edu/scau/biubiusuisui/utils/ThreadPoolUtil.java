package cn.edu.scau.biubiusuisui.utils;

import cn.edu.scau.biubiusuisui.thread.FXPlusExecutor;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * 线程池工具类
 *
 * @author Jade Yeung
 * @time 2022/4/30 17:42
 * @since 1.3.0
 */
public class ThreadPoolUtil {
    /**
     * 核心线程数默认值
     */
    private static final int DEFAULT_CORE_SIZE = Runtime.getRuntime().availableProcessors() > 1 ? Runtime.getRuntime().availableProcessors() / 2 + 1 : 2;
    private static final long DEFAULT_KEEP_ALIVE_TIME_MILLS = 300 * 1000L;
    private static final Map<String, Executor> EXECUTORS = new ConcurrentHashMap<>();
    private static final Map<String, ScheduledThreadPoolExecutor> SCHEDULED_EXECUTORS = new ConcurrentHashMap<>();

    private ThreadPoolUtil() {
    }

    public static void execute(Runnable runnable) {
        Executor executor = getExecutor(ThreadPoolUtil.class.getName() + "-common");
        executor.execute(runnable);
    }

    public static void execute(String threadName, Runnable runnable) {
        Executor executor = getExecutor(threadName);
        executor.execute(runnable);
    }

    public static ScheduledThreadPoolExecutor getScheduledExecutor(String threadName) {
        return getScheduledExecutor(DEFAULT_CORE_SIZE, threadName);
    }

    public static ScheduledThreadPoolExecutor getScheduledExecutor(int coreSize, String threadName) {
        return getScheduledExecutor(coreSize, true, threadName);
    }

    public static ScheduledThreadPoolExecutor getScheduledExecutor(int coreSize, boolean daemon, String threadName) {
        ScheduledThreadPoolExecutor scheduledExecutor = SCHEDULED_EXECUTORS.get(threadName);
        if (Objects.isNull(scheduledExecutor)) {
            scheduledExecutor = createScheduledExecutor(coreSize, daemon, threadName);
            EXECUTORS.put(threadName, scheduledExecutor);
        }
        return scheduledExecutor;
    }

    public static Executor getExecutor(String threadName) {
        return getExecutor(Integer.MAX_VALUE, threadName);
    }

    public static Executor getExecutor(int maxSize, String threadName) {
        return getExecutor(DEFAULT_CORE_SIZE, maxSize, threadName);
    }

    public static Executor getExecutor(int coreSize, int maxSize, String threadName) {
        return getExecutor(coreSize, maxSize, DEFAULT_KEEP_ALIVE_TIME_MILLS, TimeUnit.MILLISECONDS, threadName);
    }

    public static Executor getExecutor(int coreSize, int maxSize, long keepAliveTime, TimeUnit timeUnit, String threadName) {
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(maxSize);
        return getExecutor(coreSize, maxSize, keepAliveTime, timeUnit, workQueue, threadName);
    }

    public static Executor getExecutor(int coreSize, int maxSize, long keepAliveTime, TimeUnit timeUnit, BlockingQueue<Runnable> workQueue, String threadName) {
        return getExecutor(coreSize, maxSize, keepAliveTime, timeUnit, workQueue, true, threadName);
    }

    public static Executor getExecutor(int coreSize, int maxSize, long keepAliveTime, TimeUnit timeUnit, BlockingQueue<Runnable> workQueue, boolean daemon, String threadName) {
        Executor executor = EXECUTORS.get(threadName);
        if (Objects.isNull(executor)) {
            executor = createExecutor(coreSize, maxSize, keepAliveTime, timeUnit, workQueue, daemon, threadName);
            EXECUTORS.put(threadName, executor);
        }
        return executor;
    }

    private static Executor createExecutor(int coreSize, int maxSize, long keepAliveTime, TimeUnit timeUnit, BlockingQueue<Runnable> workQueue, boolean daemon, String threadName) {
        return new FXPlusExecutor(coreSize, maxSize, keepAliveTime, timeUnit, workQueue, daemon, threadName);
    }

    private static ScheduledThreadPoolExecutor createScheduledExecutor(int coreSize, boolean daemon, String threadName) {
        return new ScheduledThreadPoolExecutor(coreSize, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(daemon);
                thread.setName(threadName);
                return thread;
            }
        });
    }
}
