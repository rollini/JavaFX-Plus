package controller;

import cn.edu.scau.biubiusuisui.annotation.FXBind;
import cn.edu.scau.biubiusuisui.annotation.FXController;
import cn.edu.scau.biubiusuisui.annotation.FXData;
import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import model.Downloader;
import model.Paper;

@FXController(path = "Main.fxml")
@FXWindow(title = "论文下载器",mainStage = true,draggable = true)
public class MainController extends FXBaseController {


    @FXBind(
            {"url=${url.text}","savePath=${savePath.text}"}
    )
    @FXData
    Downloader downloader =new Downloader();


    @FXBind(
            {
                    "year=${@to_int(year.text)}","firstAuthor=${author1.text}","secondAuthor=${author2.text}",
                    "venue=${venue.text}","is_single=${is_single.selected}","name=${title.text}"
            }
    )
    @FXData
    Paper paper = new Paper();


    @FXBind(
            "progress=${downloader.download_rate}"
    )
    @FXML
    private ProgressBar progressBar;



    @FXML
    private TextField url;

    @FXML
    private TextField year;

    @FXML
    private TextField author1;

    @FXML
    private TextField author2;

    @FXML
    private Button download;

    @FXML
    private CheckBox is_single;

    @FXML
    private TextField savePath;


    @FXML
    private TextField venue;

    @FXML
    private TextField title;


    @FXML
    public void test(){
        downloader.download(paper);
    }


    public int to_int(String text){
        //System.out.println("???");
        if(!"".equals(text)) {
            return Integer.valueOf(text);
        }else{
            return 0;
        }
    }
}
