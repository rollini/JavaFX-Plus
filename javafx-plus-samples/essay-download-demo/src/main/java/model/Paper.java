package model;

import cn.edu.scau.biubiusuisui.annotation.FXEntity;
import cn.edu.scau.biubiusuisui.annotation.FXField;

@FXEntity
public class Paper {

    @FXField
    String name;
    @FXField
    String venue;
    @FXField
    String firstAuthor;
    @FXField
    String secondAuthor;
    @FXField
    Boolean is_single  = new Boolean(false);

    @FXField
    int year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getFirstAuthor() {
        return firstAuthor;
    }

    public void setFirstAuthor(String firstAuthor) {
        this.firstAuthor = firstAuthor;
    }

    public String getSecondAuthor() {
        return secondAuthor;
    }

    public void setSecondAuthor(String secondAuthor) {
        this.secondAuthor = secondAuthor;
    }

    public Boolean getIs_single() {
        return is_single;
    }

    public void setIs_single(Boolean is_single) {
        this.is_single = is_single;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        if(is_single) {
            return String.format("(%s. %s %d)%s.pdf",firstAuthor,venue,year,name);
        }else{
            if(null == secondAuthor || "".equals(secondAuthor)){
                return String.format("(%s et al. %s %d)%s.pdf",firstAuthor,venue,year,name);
            }else{
                return String.format("(%s and %s. %s %d)%s.pdf",firstAuthor,secondAuthor,venue,year,name);
            }
        }
    }
}
