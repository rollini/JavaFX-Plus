# JavaFX-Plus Samples

- 基于JavaFX-Plus框架开发的示例DEMO，DEMO版本与JavaFX-Plus版本迭代一致。
- 以Maven多Module的形式进行管理，开发人员只需要导入javafx-plus-samples/pom.xml即可。

## 模块

### simple-demo

最原始的示例DEMO，内含一系列简单的使用案例。

### essay-download-demo

论文下载器，原链接：https://gitee.com/Biubiuyuyu/JavaFX-Demo
现该DEMO已被整合至此项目。

